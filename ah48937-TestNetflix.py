#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, create_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.1\n2.9\n3.6\n0.78\n")

    #second eval test
    def test_eval_2(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
                w.getvalue(), "1:\n3.8\n3.4\n3.7\n4.8\n3.8\n4.0\n0.63\n")

     #all movies have have an rmse less than 1
    def test_precision_rmse_less_than_1(self):
        r = open("RunNetflix.in", "r")
        w = StringIO()
        rmse = netflix_eval(r, w)
        r.close()
        self.assertLessEqual(rmse, 1)

    def test_cache_3(self):
        YEAR_OF_RATING = create_cache("cache-yearCustomerRatedMovie.pickle")
        self.assertEqual(1408395, len(YEAR_OF_RATING))

    def test_check_actual_customer_rating_cache(self):
        self.assertGreater(len(create_cache("cache-actualCustomerRating.pickle")), 0)


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
